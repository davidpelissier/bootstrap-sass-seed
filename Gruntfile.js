module.exports = function(grunt) {
  grunt.initConfig({

    sass: {
      // This is the "dev" SASS config used with "grunt watch" command
      dev: {
        options: {
          style: 'expanded',
          loadPath: 'node_modules/bootstrap-sass/assets/stylesheets'
        },
        files: {
          'dist/stylesheets/style.css': 'sass/style.scss'
        }
      },

      // This is the "production" SASS config used with the "grunt buildcss" command
      dist: {
        options: {
          style: 'compressed',
          loadPath: 'node_modules/bootstrap-sass/assets/stylesheets'
        },
        files: {
          'dist/stylesheets/style.min.css': 'sass/style.scss'
        }
      }
    },

    // Configure the "grunt watch" task
    watch: {
      sass: {
        files: '**/*.scss',
        tasks: ['sass:dev']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // "grunt buildcss" is the same as running "grunt sass:dist".
  // if I had other tasks, I could add them to this array.
  grunt.registerTask('buildcss', ['sass:dist']);
};
