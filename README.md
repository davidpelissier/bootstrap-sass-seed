# bootstrap-sass-seed — La graine pour du maquettage rapide avec Bootstrap

Ce projet est une base applicative permettant un maquettage rapide d'une
web application.

L'environnement fourni va vous offrir des outils vous permettant de personnaliser
le framework de Twitter et de compiler vos fichiers de style dynamiques SASS
sans passer par une application.

Pour cela, la graine implemente des fichiers de configuration pour différents
outils, à savoir, Grunt, SASS et Node Package Manager.

![Grunt](http://s595203771.onlinehome.fr/resources/grunt-logo.png)
![SASS](http://s595203771.onlinehome.fr/resources/sass-logo.png)
![NPM](http://s595203771.onlinehome.fr/resources/npm-logo.png)

## Structure du projet


    mon-projet
    ├── dist
    │   ├── fonts
    │   ├── images
    │   ├── javascripts
    │   │   └── bootstrap.min.js
    │   └── stylesheets
    │       └── style.min.css
    ├── Gruntfile.js
    ├── node_modules
    │   ├── bootstrap-sass
    │   ├── grunt
    │   ├── grunt-contrib-sass
    │   └── grunt-contrib-watch
    ├── package.json
    └── sass
        ├── _bootstrap-variables-override.scss
        ├── _bootstrap.scss
        └── style.scss


## Pré-requis
Nous utilisons également un certain nombre d'outils de Node.js pour initialiser  notre application. Vous devez avoir node.js et son gestionnaire de paquets ( NPM) installé. Vous pouvez les obtenir à partir `http://nodejs.org/`.

Grunt est également nécessaire au bon fonctionnement de votre projet. Une fois
NPM installé, lancer la commande suivant pour installer Grunt de façon globale :

    npm install -g grunt-cli

## Cloner le dépôt
Vous devez cloner le dépôt bootstrap-sass-seed. Si vous n'avez pas Git, vous pouvez le télécharger sur `http://git-scm.com/`.

    git clone https://bitbucket.org/davidpelissier/bootstrap-sass-seed

Renommer le dossier à votre convenance.

## Installer les dépendances
Comme énoncé précedeemment, nous utilisons l'outil NPM afin de gérer les dépendnces de notre projet.
Pour initialiser les dépendances, il vous suffit d'éxécuter simplement la commande suivante à la racine de votre projet :

    npm install

## Personnaliser son installation de Bootstrap
Afin de modifier le framework Bootstrap à votre sauce, écrasez simplement les
variables par défaut de ce dernier à le fichier `_bootstrap-variables-override.scss`. **Veillez à supprimer l'attribut `!default`**.

## Compiler vos fichiers de style sass
Deux types de compilation s'offrent à vous :

  - `grunt watch` pour une compilation dans l'environnement de développement. Cette commande inspectera chaque modification engendrée sur vos fichiers SASS et compilera l'ensemble à la volée, de façon lisible dans le fichier `style.css`.

  - `grunt buildcss` pour une compilation dans l'environnement de production.
  Cette commande produira un CSS optimisé et compressé.
